rm(list = ls())

k <- 5
vcv_options <- c("full", paste("training", 1:k, sep = ""))[1]
for (cv_options in vcv_options) {

source("02_preprocess.R")

# rm na's
iNA_other <- which(is.na(fl_data$preprocessed_other))
flother_locs <- fl_locs@coords[-iNA_other, ]
this_other <- fl_data$preprocessed_other[-iNA_other]
fl_data <- fl_data[-iNA_other, ]

if (length(cv_options) != 1) {
  cv_options <- "training1"
}

if (identical(cv_options, "full")) {
  cat("all but locations which include na's are used")
  this_scales <- full_other_maternC_scales
} else {

  # set.seed(123)
  set.seed(4299877)
  klocs <- block_k_subsetting(locations = flother_locs, blocksidelength = .2, k = k, random = F)

  get_notk_klocs <- function(cvoption = "training1") {
    leavekout <- as.numeric(gsub(pattern = "training", x = cvoption, replacement = ""))
    keep <- (1:k)[-leavekout]

    return(sort(unlist(sapply(keep, FUN = function(x) { klocs[[x]] } ))))
  }

  klocsind <- get_notk_klocs(cvoption = cv_options)
  fl_data <- fl_data[klocsind, ]
  flother_locs <- flother_locs[klocsind, ]
  this_other <- this_other[klocsind]

  this_scales <- get(paste0("training",
                            as.numeric(gsub(pattern = "training", x = cv_options, replacement = "")),
                            "_other_maternC_scales"))

}

n_other <- dim(flother_locs)[1]
options(spam.nearestdistnnz = c(2^28, 1000))

  # - conditional sampling ------------------------------------------------------------------------ #
  if (!file.exists(file = paste0(CommonPath, "application/", cv_options, "_other_sample.RData"))) {
    time_conditionalsampling <- system.time({

      distmat_other <- nearest.dist(flother_locs , delta = 180, upper = FALSE, method = "greatcircle", R = 1)
      distmat_other <- distmat_other + t(distmat_other)

      Sigma_other <- (cov.finnmat(h = distmat_other,
                                  theta = c(max(distmat_other)/15, 1, 1, 1e-2)))
      Rstruct_other <- chol(Sigma_other)

      neg2loglikelihood_other <- function(theta) {
        Sigma_other <- cov.finnmat(h = distmat_other, theta = theta)
        cholS <- chol(Sigma_other, Rstruct = Rstruct_other)
        logdet <- sum(log(diag(cholS)))

        return(n_other * log(2 * pi) +
                 2 * logdet +
                 sum(this_other * backsolve(cholS,
                                            forwardsolve(cholS, this_other, transpose = TRUE,
                                                         upper.tri = TRUE), n_other)))
      }

      cl <- makeCluster(9)
      setDefaultCluster(cl = cl)
      clusterEvalQ(cl, library("spam"))
      clusterEvalQ(cl, library("spam64"))
      clusterExport(cl, c("neg2loglikelihood_other", "this_other", "distmat_other", "Rstruct_other",
                          "n_other", "cov.finnmat"))

      optout <- optimParallel::optimParallel(fn = neg2loglikelihood_other,
                                             method = "L-BFGS-B",
                                             lower = c(1e-8, 1e-6, .1, 1e-6),
                                             upper = c(max(distmat_other)/2, 10, 1.5, 1),
                                             par = c(max(distmat_other)/10, 1, 1, 1e-2),
                                             parallel = list(loginfo = TRUE, forward = FALSE),
                                             control = list(trace = TRUE) )
      stopCluster(cl)
      sampling_par<- optout$par
      cat(paste("\n conditional sampling optimout: ", paste(sampling_par, collapse =  " "), "\n"))

      SigmaXX <- as.spam(cov.finnmat(h = distmat_other, theta = sampling_par[1:3]))
      In <- diag.spam(1, n_other, n_other)
      SigmaYY <- as.spam(SigmaXX + sampling_par[4]*In)
      RstructYY <- chol.spam(SigmaYY)

      posteriorsample <- rmvnorm.conditional(n = n_posteriorsamples,
                                             SigmaXX = SigmaXX, SigmaYY = SigmaYY,
                                             RstructYY = RstructYY, y = this_other)
      posteriormean <- apply(posteriorsample, 2, mean)

    }); cat(paste("\n conditional sampling time in seconds used: ", time_conditionalsampling[3], "\n"))

    save(posteriorsample, time_conditionalsampling, sampling_par,
         n_other,
         file = paste0(CommonPath, "application/", cv_options, "_other_sample.RData"))

  } else if (TRUE) {
    load(file = paste0(CommonPath, "application/", cv_options, "_other_sample.RData"))

    posteriormean <- apply(posteriorsample, 2, mean, na.rm = TRUE)
    posteriorsd <- apply(posteriorsample, 2, sd, na.rm = TRUE)
    posteriorquantiles <- apply(posteriorsample, 2, quantile, probs = c(.05, .95), na.rm = TRUE)

    if (F) {
      x11()
      par(mfrow = c(4, 2))
      plot(posteriormean - this_other)
      plot(posteriorsd)

      this_colors <- fields::tim.colors(n = 64)[cut(this_other, 64)]
      plot(flother_locs, col = this_colors, pch = 20, cex = .5,
           yaxt = "n", xaxt = "n", ylab = "", xlab = "", main = "other basal area")
      this_colors <- fields::tim.colors(n = 64)[cut(posteriormean, 64)]
      plot(flother_locs, col = this_colors, pch = 20, cex = .5,
           yaxt = "n", xaxt = "n", ylab = "", xlab = "", main = "mean")
      this_colors <- fields::tim.colors(n = 64)[cut(posteriorsd, 64)]
      plot(flother_locs, col = this_colors, pch = 20, cex = .5,
           yaxt = "n", xaxt = "n", ylab = "", xlab = "", main = "sd")
      this_colors <- fields::tim.colors(n = 64)[cut(posteriorquantiles[, 2]-posteriorquantiles[, 1], 64)]
      plot(flother_locs, col = this_colors, pch = 20, cex = .5,
           yaxt = "n", xaxt = "n", ylab = "", xlab = "")
      indx <- 50
      plot(y = posteriormean[20:(20+indx-1)], x = 1:indx, type = "b", lty = 2, lwd = 3, ylim = c(-5, 5))
      for(lineindx in sample(1:n_posteriorsamples, 10, replace = FALSE))
        lines(y = posteriorsample[lineindx, 20:(20+indx-1)], x = 1:indx, col = lineindx, type = "b")
      lines(y = this_other[20:(20+indx-1)], x = 1:indx, col = "red", type = "l", lwd = 1.5)
    }
  }
  # ----------------------------------------------------------------------------------------------- #




  # - matern C ------------------------------------------------------------------------------------ #
  if (!file.exists(paste0(CommonPath, "application/", cv_options, "_other_maternC_scales.RData"))) {
    time_maternC <- system.time({

      distmat_other <- nearest.dist(flother_locs , delta = 180, upper = FALSE, method = "greatcircle", R = 1)
      distmat_other <- distmat_other + t(distmat_other)

      this_posteriormean <- posteriormean

      maternC_SigmaSmooth <- spam::as.spam(cov.finnmat(h = distmat_other,
                                                       theta = c(26/6378.1, 1, 0.5, 0)))

      struct_maternC_SigmaSmooth <- chol.spam(maternC_SigmaSmooth,
                                              memory = list(nnzcolindices = 10^4))
      In  <-  diag.spam(1, n_other, n_other)

      { cl <- makeCluster(n_lderiv_cores)
        setDefaultCluster(cl = cl)
        clusterEvalQ(cl, library("spam"))
        clusterEvalQ(cl, library("spam64"))
        clusterEvalQ(cl, library("mresa"))
        clusterExport(cl, c("scaleseq", "this_posteriormean", "maternC_SigmaSmooth", "struct_maternC_SigmaSmooth", "In",
                            "spatial_logderiv"))
        maternC_lderiv_m <- parSapply(cl, scaleseq, spatial_logderiv, sample = this_posteriormean,
                                      Sigma = maternC_SigmaSmooth, rstruct = struct_maternC_SigmaSmooth,
                                      In = In,
                                      norm = 1, norm.type = "m")
        stopCluster(cl) }
    })

    { cl <- makeCluster(n_lderiv_cores)
      setDefaultCluster(cl = cl)
      clusterEvalQ(cl, library("spam"))
      clusterEvalQ(cl, library("spam64"))
      clusterEvalQ(cl, library("mresa"))
      clusterExport(cl, c("scaleseq", "this_posteriormean", "maternC_SigmaSmooth", "struct_maternC_SigmaSmooth", "In",
                          "spatial_logderiv"))
      maternC_lderiv_h <- parSapply(cl, scaleseq, spatial_logderiv, sample = this_posteriormean,
                                    Sigma = maternC_SigmaSmooth, rstruct = struct_maternC_SigmaSmooth,
                                    In = In,
                                    norm = 1, norm.type = "h")
      stopCluster(cl) }

    maternC_minima_m <- inflect(maternC_lderiv_m)$minima
    maternC_minima_h <- inflect(maternC_lderiv_h)$minima

    cv_options
    plot(maternC_lderiv_m, x = log(scaleseq), type = "l")
    abline(v = log(scaleseq[maternC_minima_m]))
    ((scaleseq[maternC_minima_m]))

    save(#maternC_optout,
      time_maternC,
      maternC_lderiv_m, maternC_lderiv_h,
      maternC_minima_m, maternC_minima_h,
      file = paste0(CommonPath, "application/", cv_options, "_other_maternC_scales.RData"))

  } else {
    load(file = paste0(CommonPath, "application/", cv_options, "_other_maternC_scales.RData"))
    # plot(log(scaleseq), maternC_lderiv_m, type  = "l"); abline(v = log(scaleseq[maternC_minima_m]))
    # scaleseq[maternC_minima_m]

    distmat_other <- nearest.dist(flother_locs , delta = 180, upper = FALSE, method = "greatcircle", R = 1)
    distmat_other <- distmat_other + t(distmat_other)

    maternC_SigmaSmooth <- spam::as.spam(cov.finnmat(h = distmat_other,
                                                     # theta = c(maternC_optout$par[1:2], 1, 0) ))
                                                     theta = c(26/6378.1, 1, 0.5, 0)))

    In  <-  diag.spam(1, n_other, n_other)
    struct_maternC_SigmaSmooth <- chol.spam(maternC_SigmaSmooth + In,
                                            memory = list(nnzcolindices = 10^4))

    if (all(is.na(this_scales))) {
      this_scales <- selectscales(lderiv = maternC_lderiv_m)
    }
  }


  # - calculate details
  if (!file.exists(paste0(CommonPath, "application/", cv_options, "_other_maternC_details.RData"))) {
    timedetail_maternC <- system.time({
      maternC_details <- mresaC(posteriorSample = t(posteriorsample[ , 1:n_other]),
                                Sigma = maternC_SigmaSmooth,
                                smoothingLevels = this_scales,
                                dimGrid = c(n_other , 1),
                                returneachsmoothedsample = FALSE)

      n_details <- length(maternC_details$smMean)-2

      { graphics.off()
        pdf(paste0("../article/fl_other_", cv_options, "_details.pdf"), width = 6.8, height = 6*1.8,
            pointsize = 10, onefile = FALSE, paper = "special")

        par(mfrow = c(3, n_details), mar = c(0.01, 0.01, 1.1, 0.01), oma = c(0,0,1,0))
        # order: bottom, left, top, and right. The default is c(5.1, 4.1, 4.1, 2.1).

        colfactors <- fields::tim.colors(n = 256)[cut(unlist(maternC_details$smMean[c(1:n_details, n_details+2)]), breaks = 256)]
        for (idetail in 1:(n_details)) {
          plot(fl_1925WGS84)
          points(flother_locs, col = colfactors[1:dim(flother_locs)[1] + (idetail-1)*dim(flother_locs)[1]],
                 cex = .5, pch = 15)
          mtext(paste0("(", letters[idetail], ")"), adj = 0, line = 0.4, cex = 1)
        }

        for (idetail in 1:(n_details)) {
          pwcols <- c("blue", "grey", "red")[factor(unlist(maternC_details$hpout[[idetail]][1]), levels = c(0, 0.5, 1))]
          plot(fl_1925WGS84)
          points(flother_locs, col = pwcols,
                 cex = .5, pch = 15)
          mtext(paste0("(", letters[idetail+n_details], ")"), adj = 0, line = 0.4, cex = 1)
        }

        par(mar = c(5.1, 4.1, 4.1, 2.1))
        plot(y = maternC_lderiv_m, log(scaleseq), type = "l", xlab = "log(lambda)", ylab = "max norm")
        mtext(paste0("(", letters[1+2*n_details], ")"), adj = 0, line = 0.4, cex = 1)
        if (length(maternC_minima_m) != 0) { abline(v = log(scaleseq[maternC_minima_m]), col = "grey") }
        plot(y = maternC_lderiv_h, log(scaleseq), type = "l", xlab = "log(lambda)", ylab = "2-norm")
        mtext(paste0("(", letters[2+2*n_details], ")"), adj = 0, line = 0.4, cex = 1)
        if (length(maternC_minima_h) != 0) { abline(v = log(scaleseq[maternC_minima_h]), col = "grey") }

        dev.off() }
    })

    save(maternC_details, n_details,
         timedetail_maternC,
         file = paste0(CommonPath, "application/", cv_options, "_other_maternC_details.RData"))
  } else {
    load(file = paste0(CommonPath, "application/", cv_options, "_other_maternC_details.RData"))
  }


  if (!file.exists(paste0(CommonPath, "application/", cv_options, "_other_maternC_estimates.RData"))) {
    timeestimates_maternC <- system.time({

      iNA <- unique(c(which(is.na(fl_data$site_3type)),
                      which(is.na(fl_data$kasket1913)),
                      which(is.na(fl_data$scaled_idw1925_10)),
                      which(is.na(fl_data$grazed)) ))

      if(T) {
        scaled_idw_details <- mresaC(posteriorSample = t(matrix(fl_data$scaled_idw1925_10, ncol = n_other)),
                                  Sigma = maternC_SigmaSmooth,
                                  smoothingLevels = this_scales,
                                  dimGrid = c(n_other , 1),
                                  returneachsmoothedsample = FALSE)

        fl_data$popdens_d1 <- scaled_idw_details$smMean[[1]]
        fl_data$popdens_d2 <- scaled_idw_details$smMean[[2]]

        kasket13_details <- mresaC(posteriorSample = t(matrix(as.numeric(fl_data$kasket1913), ncol = n_other)),
                                     Sigma = maternC_SigmaSmooth,
                                     smoothingLevels = this_scales,
                                     dimGrid = c(n_other , 1),
                                     returneachsmoothedsample = FALSE)

        fl_data$kasket_d1 <- kasket13_details$smMean[[1]]
        fl_data$kasket_d2 <- kasket13_details$smMean[[2]]

        sitetype_details <- mresaC(posteriorSample = t(matrix(as.numeric(fl_data$site_3type), ncol = n_other)),
                                   Sigma = maternC_SigmaSmooth,
                                   smoothingLevels = this_scales,
                                   dimGrid = c(n_other , 1),
                                   returneachsmoothedsample = FALSE)

        fl_data$sitetype_d1 <- sitetype_details$smMean[[1]]
        fl_data$sitetype_d2 <- sitetype_details$smMean[[2]]

        grazed_details <- mresaC(posteriorSample = t(matrix(as.numeric(fl_data$grazed), ncol = n_other)),
                                   Sigma = maternC_SigmaSmooth,
                                   smoothingLevels = this_scales,
                                   dimGrid = c(n_other , 1),
                                   returneachsmoothedsample = FALSE)

        fl_data$grazed_d1 <- grazed_details$smMean[[1]]
        fl_data$grazed_d2 <- grazed_details$smMean[[2]]
      }

      subdistmat <- distmat_other[-iNA, -iNA]


      Sigma <- cov.finnmat(h = subdistmat,
                           theta = c(max(subdistmat)/10, 1, .5, 1e-6))
      Rstruct <- chol(Sigma)
      sub_n_other <- dim(subdistmat)[1]
      thetalower = c(1e-8, 1e-6, .1, 1e-8)
      thetaupper = c(.1, 3, 2, .2)



      maternC_detailpars <- list(NULL)
      for (idetail in 1:n_details) {
        this_detail <- maternC_details$smMean[[idetail]][-iNA]

        if (idetail == 1) {
          Xdesign <- model.matrix(~ sitetype_d1 + kasket_d1 + popdens_d1 + grazed_d1,
                                  data = fl_data[-iNA, ])


          detail_fit <- glm(this_detail ~ sitetype_d1 + kasket_d1 + popdens_d1 + grazed_d1,
                            data = fl_data[-iNA, ])
        }
        if (idetail == 2) {
          Xdesign <- model.matrix(~ sitetype_d2 + kasket_d2 + popdens_d2 + grazed_d2,
                                  data = fl_data[-iNA, ])


          detail_fit <- glm(this_detail ~ sitetype_d2 + kasket_d2 + popdens_d2 + grazed_d2,
                            data = fl_data[-iNA, ])
        }

        # subdistmat <- distmat_other[-iNA, -iNA]
        p <- dim(Xdesign)[2]



        beta0 <- coef(detail_fit)

        theta0 = c(max(subdistmat)/10, 1, .5, 1e-6)

        Sigma_other <- (cov.finnmat(h = subdistmat, theta = c(theta0) ))
        Rstruct <- chol.spam(Sigma_other)

        neg2loglikelihood <- function(fulltheta, ...) {

          Sigma <- cov.finnmat(h = subdistmat, c(fulltheta[-(1:p)]))
          cholS <- update.spam.chol.NgPeyton(Rstruct, Sigma, ...)
          resid <- this_detail - Xdesign %*% fulltheta[1:p]

          return(sub_n_other * log(2 * pi) + 2 * c(determinant.spam.chol.NgPeyton(cholS)$modulus) +
                   sum(resid * solve.spam(cholS, resid)))
        }

        maternC_cl <- makeCluster(2*(p+3)+1)
        setDefaultCluster(cl = maternC_cl)
        clusterEvalQ(maternC_cl, library("spam"))
        clusterEvalQ(maternC_cl, library("spam64"))
        clusterExport(maternC_cl, c("neg2loglikelihood", "this_detail", "subdistmat", "Rstruct", "sub_n_other", "Xdesign", "p", "cov.finnmat"))

        maternC_detailpars[[idetail]] <- optimParallel(fn = neg2loglikelihood,
                                                       method = "L-BFGS-B",
                                                       lower = c(rep(-100, p), thetalower),
                                                       upper = c(rep(100, p), thetaupper),
                                                       par = c(beta0, theta0),
                                                       parallel = list(loginfo = TRUE, forward = FALSE),
                                                       control = list(trace = TRUE, factr = 1e7,
                                                                      maxit = 1000),
                                                       hessian = T)
        stopCluster(maternC_cl)
        cat(paste("\n maternC detail ", idetail ," optimout: ", paste(maternC_detailpars[[idetail]]$par, collapse =  " "), "\n"))
      }
    })

    save(n_details, fl_data,
         maternC_detailpars, #maternC_detailparsbeta,
         timeestimates_maternC,
         file = paste0(CommonPath, "application/", cv_options, "_other_maternC_estimates.RData"))
  } else {
    load(file = paste0(CommonPath, "application/", cv_options, "_other_maternC_estimates.RData"))
  }
  # ----------------------------------------------------------------------------------------------- #


  ##### analysis
  #####

  if (F) {

    h <- seq(from = 1e-16, to = .8, length.out = 4000000)
    maternC_other_detail_estimates <- matrix(NA, ncol = 4, nrow = n_details)
    for (i in 1:n_details) {
      covest <- maternC_detailpars[[i]]$par[(length(maternC_detailpars[[i]]$par)-3):length(maternC_detailpars[[i]]$par)]
      maternC_other_detail_estimates[i, ] <- c(covest) }

    { graphics.off()
      pdf(paste0("../article/fl_other_", cv_options, "_details.pdf"), width = 8.3-1.5, height = 11.7-1.5,
          pointsize = 10, onefile = FALSE, paper = "special")

      par(mfrow = c(4, n_details), mar = c(0.01, 0.01, 1.1, 0.01), oma = c(0,0,1,0))
      # order: bottom, left, top, and right. The default is c(5.1, 4.1, 4.1, 2.1).

      colfactors <- fields::tim.colors(n = 256)[cut(unlist(maternC_details$smMean[c(1:n_details, n_details+2)]), breaks = 256)]
      for (idetail in 1:(n_details)) {
        plot(fl_1925WGS84, lwd = .5)
        points(flother_locs, col = colfactors[1:dim(flother_locs)[1] + (idetail-1)*dim(flother_locs)[1]],
               cex = .3, pch = 15)
        mtext(paste0("(", letters[idetail], ")"), adj = 0, line = 0.4, cex = 1)
        raster::scalebar(200, divs = 4, type = "bar", lonlat = NULL)
      }

      for (idetail in 1:(n_details)) {
        pwcols <- c("blue", "grey", "red")[factor(unlist(maternC_details$hpout[[idetail]][1]), levels = c(0, 0.5, 1))]
        plot(fl_1925WGS84, lwd = .5)
        points(flother_locs, col = pwcols,
               cex = .3, pch = 15)
        mtext(paste0("(", letters[idetail+n_details], ")"), adj = 0, line = 0.4, cex = 1)
        raster::scalebar(200, divs = 4, type = "bar", lonlat = NULL)
      }

      par(mar = c(5.1, 4.1, 4.1, 2.1))
      for (i in 1:n_details) {
        covest <- maternC_detailpars[[i]]$par[(length(maternC_detailpars[[i]]$par)-3):length(maternC_detailpars[[i]]$par)]
        plot(y = cov.finnmat(h, theta = covest), x = h*6378.1,
             type = "l", xlim = c(0, covest[1]*20*6378.1),
             xlab = "spatial lag (km)", ylab = "covariance")
        abline(v = maternC_other_detail_estimates[i, 1], lty = 2, col = "grey")
        text(x = maternC_other_detail_estimates[i, 1]*1.4*6378.1, y = maternC_other_detail_estimates[i, 2]*.8,
             labels = paste0(round(maternC_other_detail_estimates[i, 1]*6378.1, 3), " km"))
        mtext(paste0("(", letters[i+2*n_details], ")"), adj = 0, line = 0.4, cex = 1)
      }

      plot(y = maternC_lderiv_m, log(scaleseq), type = "l", xlab = "log(lambda)", ylab = "max norm")
      mtext(paste0("(", letters[1+3*n_details], ")"), adj = 0, line = 0.4, cex = 1)
      if (length(maternC_minima_m) != 0) { abline(v = log(this_scales), col = "grey") }
      plot(y = maternC_lderiv_h, log(scaleseq), type = "l", xlab = "log(lambda)", ylab = "2-norm")
      mtext(paste0("(", letters[2+3*n_details], ")"), adj = 0, line = 0.4, cex = 1)
      if (length(maternC_minima_h) != 0) { abline(v = log(scaleseq[maternC_minima_h]), col = "grey") }

      dev.off() }

  }


  if (cv_options == "full" & !file.exists(paste0(CommonPath, "application/", cv_options, "_other_maternC_interpolations.RData"))) {

    # process estimates for interpolation
    ########
    load(file = paste0(CommonPath, "application/fl_water.RData"))

    plot(fl_1925WGS84, lwd = .5)
    points(interpolationpoints[,1:2], pch = 20, cex = .01)

    interpolationpoints <- as.data.frame(interpolationpoints)
    interpolationpoints$idw10 <- minmax_scale(interpolationpoints$idw10)
    interpolationpoints$site_3type <- factor(interpolationpoints$site_3type,
                                             levels = 1:3,
                                 labels = rev(c("lehtomainen/tuore", "kuivahko", "kuiva/karukko")),
                                 ordered = TRUE)

    interpolationpoints$kasket1913 <- factor(interpolationpoints$kasket1913, ordered = T)
    interpolationpoints$grazed <- factor(interpolationpoints$grazed)


    if(T) {

      n_int <- dim(interpolationpoints)[1]
      int_distmat <- nearest.dist(interpolationpoints[,1:2] , delta = 180, upper = FALSE, method = "greatcircle", R = 1)
      int_distmat <- int_distmat + t(int_distmat)

      maternC_SigmaSmooth_int <- spam::as.spam(cov.finnmat(h = int_distmat,
                                                           theta = c(26/6378.1, 1, 0.5, 0)))

      scaled_idw_details <- mresaC(posteriorSample = t(matrix(c(interpolationpoints$idw10), nrow = 1)),
                                   Sigma = maternC_SigmaSmooth_int,
                                   smoothingLevels = this_scales,
                                   dimGrid = c(n_int, 1),
                                   returneachsmoothedsample = FALSE)

      interpolationpoints$popdens_d1 <- scaled_idw_details$smMean[[1]]
      interpolationpoints$popdens_d2 <- scaled_idw_details$smMean[[2]]

      kasket13_details <- mresaC(posteriorSample = t(matrix(c(interpolationpoints$kasket1913), nrow = 1)),
                                 Sigma = maternC_SigmaSmooth_int,
                                 smoothingLevels = this_scales,
                                 dimGrid = c(n_int, 1),
                                 returneachsmoothedsample = FALSE)
      interpolationpoints$kasket_d1 <- kasket13_details$smMean[[1]]
      interpolationpoints$kasket_d2 <- kasket13_details$smMean[[2]]

      sitetype_details <- mresaC(posteriorSample = t(matrix(c(interpolationpoints$site_3type), nrow = 1)),
                                 Sigma = maternC_SigmaSmooth_int,
                                 smoothingLevels = this_scales,
                                 dimGrid = c(n_int, 1),
                                 returneachsmoothedsample = FALSE)

      interpolationpoints$sitetype_d1 <- sitetype_details$smMean[[1]]
      interpolationpoints$sitetype_d2 <- sitetype_details$smMean[[2]]

      grazed_details <- mresaC(posteriorSample = t(matrix(c(interpolationpoints$grazed), nrow = 1)),
                               Sigma = maternC_SigmaSmooth_int,
                               smoothingLevels = this_scales,
                               dimGrid = c(n_int, 1),
                               returneachsmoothedsample = FALSE)
      interpolationpoints$grazed_d1 <- grazed_details$smMean[[1]]
      interpolationpoints$grazed_d2 <- grazed_details$smMean[[2]]
    }




    Xdesign_d1 <- model.matrix(~ sitetype_d1 + kasket_d1 + popdens_d1 + grazed_d1,
                            data = interpolationpoints)
    Xdesign_d2 <- model.matrix(~ sitetype_d2 + kasket_d2 + popdens_d2 + grazed_d2,
                               data = interpolationpoints)
    p <- dim(Xdesign_d1)[2]
    nObs <- length(c(maternC_details$smMean[[2]]))
    npred <- dim(interpolationpoints)[1]

    options(spam.nearestdistnnz = c(2^28, 1000))
    # this_klocs <- klocs[[as.numeric(gsub(pattern = "training", x = trainingset, replacement = ""))]]
    distmat <- (nearest.dist(flother_locs, delta = 180, upper = NULL, method = "greatcircle", R = 1))
    distmat0 <- (nearest.dist(interpolationpoints[, 1:2], flother_locs, delta = 180, upper = NULL, method = "greatcircle", R = 1))

    Sigma_d1 <- as.spam(cov.finnmat(distmat, theta = maternC_detailpars[[1]]$par[(p+1):(p+4)]))
    Rstruct_d1 <- chol.spam(Sigma_d1)
    SinvY_d1 <- solve.spam(Sigma_d1, maternC_details$smMean[[1]], Rstruct = Rstruct_d1)
    prediction_spatiald1 <- as.spam(cov.finnmat(distmat0, theta = maternC_detailpars[[1]]$par[(p+1):(p+4)])) %*% SinvY_d1
    pred_z1 <- c(Xdesign_d1 %*% maternC_detailpars[[1]]$par[1:p]) + c(prediction_spatiald1)
    cols_z1 <- colorspace::sequential_hcl(n= 256, "viridis", rev = F)[cut(c(pred_z1, maternC_details$smMean[[1]]), 256)]

    plot(interpolationpoints[,1:2], col = cols_z1[1:npred],
         cex = .1, pch = 20, lwd = 1.2,
         asp = 2.3, xlim = c(21, 32), ylim = c(59.9, 69.7),
         xlab = "", ylab = "",
         bty = "n", xaxt = "n", yaxt = "n")
    points(flother_locs, col = cols_z1[(npred+1):(npred+nObs)], cex = .4, pch = "|", lwd = 1.2)

    plot(fl_1925WGS84, lwd = .4, add = TRUE)
    plot(bboxwater1, add = T, col = "grey", lwd = .1)
    plot(bboxwater, add = T, col = "grey", lwd = .1)


    Sigma_d2 <- as.spam(cov.finnmat(distmat, theta = maternC_detailpars[[2]]$par[(p+1):(p+4)]))
    Rstruct_d2 <- chol.spam(Sigma_d2, memory = list(nnzcolindices = 10^5))
    SinvY_d2 <- solve(Sigma_d2, maternC_details$smMean[[2]], Rstruct = Rstruct_d2)
    prediction_spatiald2 <- cov.finnmat(distmat0, theta = maternC_detailpars[[2]]$par[(p+1):(p+4)]) %*% SinvY_d2
    pred_z2 <- c(Xdesign_d2 %*% maternC_detailpars[[2]]$par[1:p]) + c(prediction_spatiald2)
    cols_z2 <- colorspace::sequential_hcl(n= 256, "viridis", rev = F)[cut(c(pred_z2, maternC_details$smMean[[2]]), 256)]
                                              # breaks = seq(-500, 100, length.out = 256), include.lowest = T, include.highest = T)]

    plot(interpolationpoints[,1:2], col = cols_z2[1:npred],
         cex = .1, pch = 20, lwd = 1.2,
         asp = 2.3, xlim = c(21, 32), ylim = c(59.9, 69.7),
         xlab = "", ylab = "",
         bty = "n", xaxt = "n", yaxt = "n")
    points(flother_locs, col = cols_z2[(npred+1):(npred+nObs)], cex = .4, pch = "|", lwd = 1.2)
    plot(fl_1925WGS84, lwd = .4, add = TRUE)
    plot(bboxwater1, add = T, col = "grey", lwd = .1)
    plot(bboxwater, add = T, col = "grey", lwd = .1)

    save(pred_z1, pred_z2,
         file = paste0(CommonPath, "application/", cv_options, "_other_maternC_interpolations.RData"))
  } else {
    load(file = paste0(CommonPath, "application/", cv_options, "_other_maternC_interpolations.RData"))
  }
}

