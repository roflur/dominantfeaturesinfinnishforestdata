rm(list = ls())
source("02_preprocess.R")

k <- 5
cv_options <- c("full", paste("training", 1:k, sep = ""))[1]


##### analysis
if (T) {
  # load pine results
  load(file = paste0(CommonPath, "application/", cv_options, "_pine_sample.RData"))
  load(file = paste0(CommonPath, "application/", cv_options, "_pine_maternC_scales.RData"))
  load(file = paste0(CommonPath, "application/", cv_options, "_pine_maternC_details.RData"))
  load(file = paste0(CommonPath, "application/", cv_options, "_pine_maternC_estimates.RData"))

  maternC_details__pine <- maternC_details

  h <- seq(from = 1e-16, to = .8, length.out = 3000000)

  p <- length(maternC_detailpars[[1]]$par)-4

  maternC_pine_detail_est <- matrix(NA, ncol = p, nrow = n_details)
  maternC_pine_detail_covest <- matrix(NA, ncol = 4, nrow = n_details)
  maternC_pine_detail_est_upper <- matrix(NA, ncol = p+4, nrow = n_details)
  maternC_pine_detail_est_lower <- matrix(NA, ncol = p+4, nrow = n_details)
  for (i in 1:n_details) {
    maternC_pine_detail_est[i, ] <- maternC_detailpars[[i]]$par[1:p]
    maternC_pine_detail_covest[i, 1:4] <- maternC_detailpars[[i]]$par[(p+1):length(maternC_detailpars[[i]]$par)]
    # maternC_pine_detail_covest[i, 5] <- get_effrange(covmat_y = cov.mat(h, theta = maternC_pine_detail_covest[i, 1:4]), h = h)

    sds <- sqrt(abs(diag(solve(maternC_detailpars[[i]]$hessian)) ))

    maternC_pine_detail_est_upper[i, ] <- maternC_detailpars[[i]]$par + qnorm(.975)*sds
    maternC_pine_detail_est_lower[i, ] <- maternC_detailpars[[i]]$par - qnorm(.975)*sds
  }


  # load spruce results
  load(file = paste0(CommonPath, "application/", cv_options, "_spruce_sample.RData"))
  load(file = paste0(CommonPath, "application/", cv_options, "_spruce_maternC_scales.RData"))
  load(file = paste0(CommonPath, "application/", cv_options, "_spruce_maternC_details.RData"))
  load(file = paste0(CommonPath, "application/", cv_options, "_spruce_maternC_estimates.RData"))

  maternC_details__spruce <- maternC_details

  h <- seq(from = 1e-16, to = .8, length.out = 3000000)
  p <- length(maternC_detailpars[[1]]$par)-4

  maternC_spruce_detail_est <- matrix(NA, ncol = p, nrow = n_details)
  maternC_spruce_detail_covest <- matrix(NA, ncol = 4, nrow = n_details)
  maternC_spruce_detail_est_upper <- matrix(NA, ncol = p+4, nrow = n_details)
  maternC_spruce_detail_est_lower <- matrix(NA, ncol = p+4, nrow = n_details)
  for (i in 1:n_details) {
    maternC_spruce_detail_est[i, ] <- maternC_detailpars[[i]]$par[1:p]
    maternC_spruce_detail_covest[i, 1:4] <- maternC_detailpars[[i]]$par[(p+1):length(maternC_detailpars[[i]]$par)]
    # maternC_spruce_detail_covest[i, 5] <- get_effrange(covmat_y = cov.mat(h, theta = maternC_spruce_detail_covest[i, 1:4]), h = h)

    sds <- sqrt(abs(diag(solve(maternC_detailpars[[i]]$hessian)) ))

    maternC_spruce_detail_est_upper[i, ] <- maternC_detailpars[[i]]$par + qnorm(.975)*sds
    maternC_spruce_detail_est_lower[i, ] <- maternC_detailpars[[i]]$par - qnorm(.975)*sds
  }


  # load birch results
  load(file = paste0(CommonPath, "application/", cv_options, "_birch_sample.RData"))
  load(file = paste0(CommonPath, "application/", cv_options, "_birch_maternC_scales.RData"))
  load(file = paste0(CommonPath, "application/", cv_options, "_birch_maternC_details.RData"))
  load(file = paste0(CommonPath, "application/", cv_options, "_birch_maternC_estimates.RData"))

  maternC_details__birch <- maternC_details

  h <- seq(from = 1e-16, to = .8, length.out = 3000000)
 
  p <- length(maternC_detailpars[[1]]$par)-4

  maternC_birch_detail_est <- matrix(NA, ncol = p, nrow = n_details)
  maternC_birch_detail_covest <- matrix(NA, ncol = 4, nrow = n_details)
  maternC_birch_detail_est_upper <- matrix(NA, ncol = p+4, nrow = n_details)
  maternC_birch_detail_est_lower <- matrix(NA, ncol = p+4, nrow = n_details)
  for (i in 1:n_details) {
    maternC_birch_detail_est[i, ] <- maternC_detailpars[[i]]$par[1:p]
    maternC_birch_detail_covest[i, 1:4] <- maternC_detailpars[[i]]$par[(p+1):length(maternC_detailpars[[i]]$par)]
    # maternC_birch_detail_covest[i, 5] <- get_effrange(covmat_y = cov.mat(h, theta = maternC_birch_detail_covest[i, 1:4]), h = h)

    sds <- sqrt(abs(diag(solve(maternC_detailpars[[i]]$hessian)) ))

    maternC_birch_detail_est_upper[i, ] <- maternC_detailpars[[i]]$par + qnorm(.975)*sds
    maternC_birch_detail_est_lower[i, ] <- maternC_detailpars[[i]]$par - qnorm(.975)*sds
  }


  # load other results
  load(file = paste0(CommonPath, "application/", cv_options, "_other_sample.RData"))
  load(file = paste0(CommonPath, "application/", cv_options, "_other_maternC_scales.RData"))
  load(file = paste0(CommonPath, "application/", cv_options, "_other_maternC_details.RData"))
  load(file = paste0(CommonPath, "application/", cv_options, "_other_maternC_estimates.RData"))

  maternC_details__other <- maternC_details

  h <- seq(from = 1e-16, to = .8, length.out = 3000000)

  p <- length(maternC_detailpars[[1]]$par)-4

  maternC_other_detail_est <- matrix(NA, ncol = p, nrow = n_details)
  maternC_other_detail_covest <- matrix(NA, ncol = 4, nrow = n_details)
  maternC_other_detail_est_upper <- matrix(NA, ncol = p+4, nrow = n_details)
  maternC_other_detail_est_lower <- matrix(NA, ncol = p+4, nrow = n_details)
  for (i in 1:n_details) {
    maternC_other_detail_est[i, ] <- maternC_detailpars[[i]]$par[1:p]
    maternC_other_detail_covest[i, 1:4] <- maternC_detailpars[[i]]$par[(p+1):length(maternC_detailpars[[i]]$par)]
    # maternC_other_detail_covest[i, 5] <- get_effrange(covmat_y = cov.mat(h, theta = maternC_other_detail_covest[i, 1:4]), h = h)

    sds <- sqrt(abs(diag(solve(maternC_detailpars[[i]]$hessian)) ))

    maternC_other_detail_est_upper[i, ] <- maternC_detailpars[[i]]$par + qnorm(.975)*sds
    maternC_other_detail_est_lower[i, ] <- maternC_detailpars[[i]]$par - qnorm(.975)*sds
  }


  detail1_pine_signest <- apply(cbind(maternC_pine_detail_est_lower[1, 1:p], maternC_pine_detail_est_upper[1, 1:p]), 1,
                                  FUN = function(x) { if (sign(x[1]) == sign(x[2])) { return(T) } else { return(F) } })
  detail2_pine_signest <- apply(cbind(maternC_pine_detail_est_lower[2, 1:p], maternC_pine_detail_est_upper[2, 1:p]), 1,
                                  FUN = function(x) { if (sign(x[1]) == sign(x[2])) { return(T) } else { return(F) } })
  detail1_spruce_signest <- apply(cbind(maternC_spruce_detail_est_lower[1, 1:p], maternC_spruce_detail_est_upper[1, 1:p]), 1,
                                  FUN = function(x) { if (sign(x[1]) == sign(x[2])) { return(T) } else { return(F) } })
  detail2_spruce_signest <- apply(cbind(maternC_spruce_detail_est_lower[2, 1:p], maternC_spruce_detail_est_upper[2, 1:p]), 1,
                                  FUN = function(x) { if (sign(x[1]) == sign(x[2])) { return(T) } else { return(F) } })
  detail1_birch_signest <- apply(cbind(maternC_birch_detail_est_lower[1, 1:p], maternC_birch_detail_est_upper[1, 1:p]), 1,
                                  FUN = function(x) { if (sign(x[1]) == sign(x[2])) { return(T) } else { return(F) } })
  detail2_birch_signest <- apply(cbind(maternC_birch_detail_est_lower[2, 1:p], maternC_birch_detail_est_upper[2, 1:p]), 1,
                                  FUN = function(x) { if (sign(x[1]) == sign(x[2])) { return(T) } else { return(F) } })
  detail1_other_signest <- apply(cbind(maternC_other_detail_est_lower[1, 1:p], maternC_other_detail_est_upper[1, 1:p]), 1,
                                 FUN = function(x) { if (sign(x[1]) == sign(x[2])) { return(T) } else { return(F) } })
  detail2_other_signest <- apply(cbind(maternC_other_detail_est_lower[2, 1:p], maternC_other_detail_est_upper[2, 1:p]), 1,
                                 FUN = function(x) { if (sign(x[1]) == sign(x[2])) { return(T) } else { return(F) } })


  sign_estimate_heatmatrix <- estimate_heatmatrix <- matrix(NA, nrow = p, ncol = n_details*4)
  estimate_heatmatrix[, 1] <- maternC_pine_detail_est[1, 1:p]
  sign_estimate_heatmatrix[detail1_pine_signest,1] <- estimate_heatmatrix[detail1_pine_signest, 1]
  estimate_heatmatrix[, 2] <- maternC_pine_detail_est[2, 1:p]
  sign_estimate_heatmatrix[detail2_pine_signest, 2] <- estimate_heatmatrix[detail2_pine_signest, 2]
  estimate_heatmatrix[, 3] <- maternC_spruce_detail_est[1, 1:p]
  sign_estimate_heatmatrix[detail1_spruce_signest,3] <- estimate_heatmatrix[detail1_spruce_signest, 3]
  estimate_heatmatrix[, 4] <- maternC_spruce_detail_est[2, 1:p]
  sign_estimate_heatmatrix[detail2_spruce_signest,4] <- estimate_heatmatrix[detail2_spruce_signest, 4]
  estimate_heatmatrix[, 5] <- maternC_birch_detail_est[1, 1:p]
  sign_estimate_heatmatrix[detail1_birch_signest,5] <- estimate_heatmatrix[detail1_birch_signest, 5]
  estimate_heatmatrix[, 6] <- maternC_birch_detail_est[2, 1:p]
  sign_estimate_heatmatrix[detail2_birch_signest,6] <- estimate_heatmatrix[detail2_birch_signest, 6]
  estimate_heatmatrix[, 7] <- maternC_other_detail_est[1, 1:p]
  sign_estimate_heatmatrix[detail1_other_signest,7] <- estimate_heatmatrix[detail1_other_signest, 7]
  estimate_heatmatrix[, 8] <- maternC_other_detail_est[2, 1:p]
  sign_estimate_heatmatrix[detail2_other_signest,8] <- estimate_heatmatrix[detail2_other_signest, 8]


  predictornames <- rownames(maternC_detailpars[[1]]$hessian)[1:p]
  rownames(sign_estimate_heatmatrix) <- predictornames[1:p]
  colnames(sign_estimate_heatmatrix) <- c("pine-detail1", "pine-detail2", "spruce-detail1", "spruce-detail2",
                                          "birch-detail1", "birch-detail2", "other-detail1", "other-detail2")
  rownames(estimate_heatmatrix) <- predictornames[1:p]
  colnames(estimate_heatmatrix) <- c("pine-detail1", "pine-detail2", "spruce-detail1", "spruce-detail2",
                                     "birch-detail1", "birch-detail2", "other-detail1", "other-detail2")
  # estimate_heatmatrix <- estimate_heatmatrix[-c((p+1):(p+5)), ]


if (F) { graphics.off()
  pdf("../article/application_estimate_heatplot.pdf", width = 6.8*2, height = 6,
      pointsize = 10, onefile = FALSE, paper = "special")

  par(mar = c(7.1, 7.1, 1.1, 7.1))
  image.plot(x = 1:nrow(sign_estimate_heatmatrix),
             y = 1:ncol(sign_estimate_heatmatrix),
             z = (sign_estimate_heatmatrix),
             zlim = c(-max(sign_estimate_heatmatrix, na.rm = T), max(sign_estimate_heatmatrix, na.rm = T)),
             xaxt = "n",
             yaxt = "n",
             ylab = "",
             xlab = "",
             las = 1,
             bty = "n",
             # horizontal      =  TRUE,
             # nlevel          =  length(levelBuffer),
             # axis.args       =  list(labels =  levelBuffer[nlevelE],
             #                         at     =  1:length(nlevelE),
             #                         font   =  1),
             col = colorspace::diverge_hcl(256, "Cork") )
             # col = colorspace::sequential_hcl(256, "plasma") )
  textgrid <- expand.grid(1:nrow(estimate_heatmatrix),
              1:ncol(estimate_heatmatrix))
  text(textgrid[,1], textgrid[,2], round(estimate_heatmatrix, 4))

  abline(v = 0:nrow(sign_estimate_heatmatrix) + 0.5,
         col = "white")
  abline(h =  0:ncol(sign_estimate_heatmatrix) + 0.5,
         col = "white")
  axis(side = 1,
       at = 1:nrow(sign_estimate_heatmatrix),
       labels = rownames(sign_estimate_heatmatrix),
       lwd = 0,
       las = 2)
  axis(side = 2,
       at = 1:ncol(sign_estimate_heatmatrix),
       labels = colnames(sign_estimate_heatmatrix),
       lwd = 0,
       las = 1)

  dev.off()

}






  ### FIGURE RESULTS APPLICATION

  if (F){ graphics.off()
    pdf(paste0("../article/fl_details", cv_options, "_details.pdf"), width = 6.8, height = 6.8*3/2,
        pointsize = 10, onefile = FALSE, paper = "special")

    nf <- layout(matrix(c(1,   2,  3,  4,  5,
                          6,   7,  8,  9, 10,
                          11, 11, 11, 12, 12), nrow = 3, ncol = 5, byrow = T),
                 widths = rep(6.8/5, 5), heights = c(2, 2, .5))
    layout.show(nf)

    par(oma = c(0, 0, 1, 0), # order: bottom, left, top, and right. The default is c(5.1, 4.1, 4.1, 2.1).
        las = 1)

    par(mar = c(0.01, 0.01, 1.1, 0.01))
    all_data <- c(fl_data$preprocessed_pine, maternC_details__pine$smMean[[1]], maternC_details__pine$smMean[[2]],
                  fl_data$preprocessed_spruce, maternC_details__spruce$smMean[[1]], maternC_details__spruce$smMean[[2]])
    all_data_lengths <- c(length(fl_data$preprocessed_pine),
                          length(maternC_details__pine$smMean[[1]]),
                          length(maternC_details__pine$smMean[[2]]),
                          length(fl_data$preprocessed_spruce),
                          length(maternC_details__spruce$smMean[[1]]),
                          length(maternC_details__spruce$smMean[[2]]))
    colfactors <- fields::tim.colors(n = 256)[cut(unlist(all_data), breaks = 256)]



    plot(fl_locs@coords, col = colfactors[1:all_data_lengths[1]],
           cex = .3, pch = 15)
    mtext(paste0("(a)"), adj = 0, line = 0.4, cex = 1)
    plot(fl_1925WGS84, lwd = .5, add = TRUE)




    for (idetail in 1:(n_details)) {
      pwcols <- c("blue", "grey", "red")[factor(unlist(maternC_details$hpout[[idetail]][1]), levels = c(0, 0.5, 1))]
      plot(fl_1925WGS84, lwd = .5)
      points(flspruce_locs, col = pwcols,
             cex = .3, pch = 15)
      mtext(paste0("(", letters[idetail+n_details], ")"), adj = 0, line = 0.4, cex = 1)
      raster::scalebar(200, divs = 4, type = "bar", lonlat = NULL)
    }

  }



}




