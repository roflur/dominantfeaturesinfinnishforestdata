# SpatialFeatureIdentification

This gitlab repository contains the R-code which was used for the analysis of Finnish forest inventory data in "Flury R., Aakala T. Ruha L., Kuuluvainen T. and Furrer R. (2022). Dominant feature identification in data from Gaussian processes applied to Finnish forest inventory records".

### how to install devel packages
  The directory source contains the version of the mresa devel R package which was used for this analysis.

  It can be installed in R with the function, e.g. `install.packages("source/mresa_1.0.9000.tar.gz", repos=NULL)`.

 
### R-Code

 The analysis directory contains the R scripts for the analysis of the Finnish forest invoentory data. Thereby the following scripts important puprpose:
 
 * 00system.R - used packages are loaded, path and globally used variables set.
 
 * 01loaddata.R - data are loaded, and lat/lon coordinates transformed.
 
 * 02preprocess.R - code to linearly detrend data for North-South trend, FL shapefiles used to define dense interpolation points, and data visualization plots.
 
 * 03a_pine.R/03b_spruce.R/03c_birch.R/03c_other.R - files to identify dominant features for the basal areas of the for individual tree species.

 * 04a_results.R/04b_results_cv.R - files to collect (intermediate) results, cross-validation results and predicting to interpolation locations.
 
 * 05_plots.R - code to plot FL maps in the article.


for further questions, contact roman.flury@math.uzh.ch or reinhard.furrer@math.uzh.ch

